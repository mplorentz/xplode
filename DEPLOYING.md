To deploy a new version of xplode to homebrew:
* tag commit to be released in xplode repo
* `git push --tags`
* open Formula/xplode.rb in mplorentz/tap repository
* Replace tag string in old `url` with new tag
* Compute a shasum of the new payload `wget -qO- {tar_gz_url} | shasum -a 256 | pbcopy`. Put it in the `sha256` field.
* Commit & push xplode.rb
